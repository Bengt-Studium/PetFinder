package petfinder.exception;

public class PetFinderException extends Exception {
    public PetFinderException(String message) {
        super(message);
    }
}
