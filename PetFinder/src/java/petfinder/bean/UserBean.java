package petfinder.bean;

public class UserBean {
    private int id;
    private String name;
    private String password;
    private String mailAddress;
    private boolean isLoggedIn;
    
    public UserBean () {};

    public int getId() {
        return id;
    }

    public boolean getIsLoggedIn() {
        return isLoggedIn;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
