package petfinder.bean;

import java.sql.Timestamp;

public class EventBean {
    private int id;
    private short type;
    private Timestamp timestamp;
    private float longitude;
    private float latitude;
    private int actor1;
    private int actor2;
    private String content;
    
    public int getActor1() {
        return actor1;
    }

    public int getActor2() {
        return actor2;
    }

    public String getContent() {
        return content;
    }

    public int getId() {
        return id;
    }

    public float getLongitude() {
        return longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public short getType() {
        return type;
    }

    public void setActor1(int actor1) {
        this.actor1 = actor1;
    }

    public void setActor2(int actor2) {
        this.actor2 = actor2;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public void setType(short type) {
        this.type = type;
    }

}
