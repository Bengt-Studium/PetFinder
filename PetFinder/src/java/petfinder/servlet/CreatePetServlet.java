package petfinder.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import petfinder.bean.AnimalBean;
import petfinder.db.AnimalManagement;
import petfinder.exception.PetFinderException;

public class CreatePetServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        AnimalBean newAnimal = new AnimalBean();
        newAnimal.setName(request.getParameter("name"));
        newAnimal.setImageURL(request.getParameter("image"));
        newAnimal.setPlace(request.getParameter("place"));
        int userID = Integer.parseInt(request.getParameter("userId"));
        try {
            AnimalManagement.createNewAnimal(userID, newAnimal);
        } catch (PetFinderException ex) {
            sendErrorRedirect(request, response, ex);
        }
        response.sendRedirect("Dashboard.jsp");
    }

    protected void sendErrorRedirect(HttpServletRequest request,
            HttpServletResponse response, PetFinderException pfex)
            throws ServletException, IOException {
        request.setAttribute("javax.servlet.jsp.jspException", pfex);
        getServletConfig().getServletContext().
                getRequestDispatcher("/simpleErrorPage.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
