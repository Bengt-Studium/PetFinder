package petfinder.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import petfinder.bean.UserBean;
import petfinder.db.UserManagement;
import petfinder.exception.PetFinderException;

public class RegistrationServlet extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (null != session.getAttribute("currentSessionUser")) {
            sendErrorRedirect(request, response, new PetFinderException(
                    "You have to log out before you can register a new user."));
            return;
        }

        UserBean newUser = new UserBean();
        newUser.setName((String)request.getParameter("username"));
        newUser.setMailAddress((String)request.getParameter("mail"));
        newUser.setPassword((String)request.getParameter("password"));
        newUser = UserManagement.register(newUser);
        if (newUser != null) {
            newUser = UserManagement.login(newUser);
            if (newUser != null) {
                session.setAttribute("currentSessionUser", newUser);
                response.sendRedirect("Dashboard.jsp");
            }
        } else {
            sendErrorRedirect(request, response, new PetFinderException(
                    "An error occured while inserting the new user into the database."));
            return;
        }
    }

    protected void sendErrorRedirect(HttpServletRequest request,
            HttpServletResponse response, PetFinderException pfex)
            throws ServletException, IOException {
        request.setAttribute("javax.servlet.jsp.jspException", pfex);
        getServletConfig().getServletContext().
                getRequestDispatcher("/simpleErrorPage.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
