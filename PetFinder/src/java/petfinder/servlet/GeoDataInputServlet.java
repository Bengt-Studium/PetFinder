package petfinder.servlet;

import java.io.IOException;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import petfinder.shared.GeoData;
import java.sql.Timestamp;
import petfinder.bean.GeoDataBean;

public class GeoDataInputServlet extends HttpServlet {
   
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder;
        //Vorbereiten des Document Builders
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            System.err.println("Failed to configure document parser.");
            return;
        }
        Document document;
        //Document Builder liest die XML im servlet request ein,
        //liefert W3C DOM Document Objekt
        try {
            document = builder.parse(request.getInputStream());
        } catch (SAXException ex) {
            System.err.println("Failed to parse input stream.");
            return;
        }
        String sensorName = document.getElementsByTagName("scai:SensorName").item(0).getTextContent();
        String[] sensorNameArray = sensorName.split("_");
        int arrayLength = sensorNameArray.length;
        //Identifizieren des Meet Events über den _Friend Suffix
        if (arrayLength>1 && sensorNameArray[arrayLength - 1].equals("Friend")) {
            //Entering meet event processing
            processMeet(document);
        } else {
            //Entering position update processing
            processPosition(document);
        }

    } 

    void processPosition(Document document) {
        //prepare GeoData object
        GeoData data = new GeoData();
        data.setType("position");
        String timestamp = document.getElementsByTagName("scai:timeStamp").item(0).getTextContent();
        data.setTimestamp(parseTimestamp(timestamp));
        data.setSensorName(document.getElementsByTagName("scai:SensorName").item(0).getTextContent());
        NodeList nodes = document.getElementsByTagName("scai:DataStreamElement");
        //Einlesen des Pet Namens
        if(nodes.item(0).getAttributes().item(0).getTextContent().equals("name")) {
            data.setName(nodes.item(0).getFirstChild().getTextContent());
        } else {
            malformedXML();
            return;
        }
        //Einlesen der Latitude
        if(nodes.item(2).getAttributes().item(0).getTextContent().equals("latitude")) {
            try {
                data.setLatitude(Double.parseDouble(nodes.item(2).getFirstChild().getTextContent()));
            } catch(NumberFormatException e) {
                System.err.println("Converting to double failed.");
            }
        } else {
            malformedXML();
            return;
        }        
        //Einlesen der Longitude
        if(nodes.item(1).getAttributes().item(0).getTextContent().equals("longitude")) {
            try {
                data.setLongitude(Double.parseDouble(nodes.item(1).getFirstChild().getTextContent()));
            } catch(NumberFormatException e) {
                System.err.println("Converting to double failed.");
            }
        } else {
            malformedXML();
            return;
        }
        sendPosition(data);
    }

    void processMeet(Document document) {
        //prepare GeoData object
        GeoData data = new GeoData();
        data.setType("meet");
        String timestamp = document.getElementsByTagName("scai:timeStamp").item(0).getTextContent();
        data.setTimestamp(parseTimestamp(timestamp));
        data.setSensorName(document.getElementsByTagName("scai:SensorName").item(0).getTextContent());
        NodeList nodes = document.getElementsByTagName("scai:DataStreamElement");

        if(nodes.item(0).getAttributes().item(0).getTextContent().equals("thisname")) {
            data.setName(nodes.item(0).getFirstChild().getTextContent());
        } else {
            malformedXML();
            return;
        }

        if(nodes.item(2).getAttributes().item(0).getTextContent().equals("thislatitude")) {
            try {
                data.setLatitude(Double.parseDouble(nodes.item(2).getFirstChild().getTextContent()));
            } catch(NumberFormatException e) {
                System.err.println("Converting to double failed.");
            }
        } else {
            malformedXML();
            return;
        }


        if(nodes.item(1).getAttributes().item(0).getTextContent().equals("thislongitude")) {
            try {
                data.setLongitude(Double.parseDouble(nodes.item(1).getFirstChild().getTextContent()));
            } catch(NumberFormatException e) {
                System.err.println("Converting to double failed.");
            }
        } else {
            malformedXML();
            return;
        }

        if(nodes.item(3).getAttributes().item(0).getTextContent().equals("othername")) {
            data.setOthername(nodes.item(3).getFirstChild().getTextContent());
        } else {
            malformedXML();
            return;
        }

        if(nodes.item(5).getAttributes().item(0).getTextContent().equals("otherlatitude")) {
            try {
                data.setOtherlatitude(Double.parseDouble(nodes.item(5).getFirstChild().getTextContent()));
            } catch(NumberFormatException e) {
                System.err.println("Converting to double failed.");
            }
        } else {
            malformedXML();
            return;
        }

        if(nodes.item(4).getAttributes().item(0).getTextContent().equals("otherlongitude")) {
            try {
                data.setOtherlongitude(Double.parseDouble(nodes.item(4).getFirstChild().getTextContent()));
            } catch(NumberFormatException e) {
                System.err.println("Converting to double failed.");
            }
        } else {
            malformedXML();
            return;
        }
        sendMeet(data);
    }

    void malformedXML() {
        System.out.println("Malformed or unexpected data structure detected. Aborting.");
        return;
    }

    long parseTimestamp(String timestamp) {
        Calendar test = javax.xml.bind.DatatypeConverter.parseDateTime(timestamp);
        long result = test.getTimeInMillis();
        return result;
    }

    void sendPosition(GeoData data) {
        GeoDataBean bean = new GeoDataBean();
        bean.setAnimalID(Integer.parseInt(data.getName().substring(3))+1);
        bean.setLatitude((float) data.getLatitude());
        bean.setLongitude((float) data.getLongitude());
        bean.setTime(new Timestamp(data.getTimestamp()));
        petfinder.db.GeoDataManagement.insertGeoData(bean);
    }

    void sendMeet(GeoData data) {
        petfinder.db.EventManagement.insertMeeting(new Timestamp(data.getTimestamp()), (float) data.getLongitude(), (float) data.getLatitude(), Integer.parseInt(data.getName().substring(3))+1, Integer.parseInt(data.getOthername().substring(3))+1);
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
      //  processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       // processRequest(request, response);
    }


    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
