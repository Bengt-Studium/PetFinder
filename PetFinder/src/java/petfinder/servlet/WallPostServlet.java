package petfinder.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import petfinder.db.EventManagement;
import petfinder.exception.PetFinderException;

public class WallPostServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (null == session.getAttribute("currentSessionUser")) {
            sendErrorRedirect(request, response, new PetFinderException(
                    "You have to log in to post on somebody's wall."));
            return;
        }

        try {
            String content = request.getParameter("text");
            int from_id = Integer.parseInt(request.getParameter("from"));
            int to_id = Integer.parseInt(request.getParameter("to"));
            if (content == null || content.equals("")) {
                sendErrorRedirect(request, response, new PetFinderException(
                    "You cannot post an empty text."));
                return;
            }

            EventManagement.insertWallPost(from_id, to_id, content);
           // response.sendRedirect("Profile.jsp?id=" + to_id);
        } catch (NumberFormatException nfe) {
            sendErrorRedirect(request, response, new PetFinderException(
                    "You provided an invalid id."));
            return;
        } catch (NullPointerException npe) {
            sendErrorRedirect(request, response, new PetFinderException(
                    "You did not provide an id."));
            return;
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    protected void sendErrorRedirect(HttpServletRequest request,
            HttpServletResponse response, PetFinderException pfex)
            throws ServletException, IOException {
        request.setAttribute("javax.servlet.jsp.jspException", pfex);
        getServletConfig().getServletContext().
                getRequestDispatcher("/simpleErrorPage.jsp").forward(request, response);
    }
}
