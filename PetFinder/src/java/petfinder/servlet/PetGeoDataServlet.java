package petfinder.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import petfinder.bean.AnimalBean;
import petfinder.bean.GeoDataBean;
import petfinder.db.AnimalManagement;

public class PetGeoDataServlet extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        int id = Integer.parseInt(request.getParameter("id"));
        AnimalBean animal = AnimalManagement.getAnimal(id);
        GeoDataBean pos = AnimalManagement.getCurrentPosition(id);
        out.println(bakeJS(animal.getId(), animal.getName(), pos.getLongitude(), pos.getLatitude()));
    }

    private String bakeJS(int id, String name, float longitude,
            float latitude) {
        return
            "var marker" + id +" = new google.maps.Marker({\n" +
                "position: new google.maps.LatLng(" + latitude + ", " + longitude + "),\n" +
                "map: map,\n" +
                "title:\"" + name + "\",\n" +
                "icon: image\n" +
            "});\n"
        ;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       // processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
