package petfinder.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import petfinder.bean.AnimalBean;
import petfinder.bean.UserBean;
import petfinder.db.UserManagement;
import petfinder.exception.PetFinderException;

public class LoginServlet extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            UserBean userBean = new UserBean();
            userBean.setName((String)request.getParameter("username"));
            userBean.setPassword((String)request.getParameter("password"));
            userBean = UserManagement.login(userBean);

            if (userBean == null) {

                sendErrorRedirect(request, response, 
                    new PetFinderException("The database returned an empty user."));
            } else if (userBean.getIsLoggedIn()) {
                HttpSession session = request.getSession();
                session.setAttribute("currentSessionUser", userBean);
                ArrayList<AnimalBean> animals = UserManagement.getAnimals(userBean.getId());
                if (animals != null && !animals.isEmpty()) {
                    session.setAttribute("aID", animals.get(0).getId());
                } else {
                    session.setAttribute("aID", null);
                }
                response.sendRedirect("Dashboard.jsp");
            } else {
                sendErrorRedirect(request, response,
                    new PetFinderException("Login failed. Unknown username or Password."));
            }
        } catch(Throwable t) {
            StackTraceElement[] s = t.getStackTrace();
            for (int i = 0; i < s.length; i++) {
                out.println(s[i].toString());
            }
            sendErrorRedirect(request, response,
                    new PetFinderException("Login failed. (Reason unknown)"));
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void sendErrorRedirect(HttpServletRequest request,
            HttpServletResponse response, PetFinderException pfex)
            throws ServletException, IOException {
        request.setAttribute("javax.servlet.jsp.jspException", pfex);
        getServletConfig().getServletContext().
                getRequestDispatcher("/simpleErrorPage.jsp").forward(request, response);
    }
}
