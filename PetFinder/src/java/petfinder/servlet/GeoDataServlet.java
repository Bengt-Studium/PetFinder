package petfinder.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import petfinder.bean.AnimalBean;
import petfinder.bean.GeoDataBean;
import petfinder.db.AnimalManagement;
import petfinder.db.GeoDataManagement;

public class GeoDataServlet extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            ArrayList<GeoDataBean> geoDataSet = GeoDataManagement.getGeoData();
            for (int i = 0; i < geoDataSet.size(); i++) {
                AnimalBean animal = AnimalManagement
                        .getAnimal(geoDataSet.get(i).getAnimalID());
                out.println(bakeJS(animal.getId(), animal.getName(),
                        animal.getImageURL(), geoDataSet.get(i).getLongitude(),
                        geoDataSet.get(i).getLatitude()));
            }
        } catch (Exception e) {
            System.err.println("GeoDataServlet: Unknown exception caught.");
        }
    }

    private String bakeJS(int id, String name, String imgurl, float longitude,
            float latitude) {
        return
            "var contentString" + id + " = " +
            "'<a href=\"Profile.jsp?id=" + id + "\">" +
                "<h2>" + name + "</h2>" +
                "<img heigth=\"128px\" width=\"128px\" src=\"" + imgurl + "\"><br/>" +
            "</a>';\n" +
            "var infowindow" + id + " = " +
                "new google.maps.InfoWindow({" +
                    "content: contentString" + id +
                 "});\n" +
            "var marker" + id +" = new google.maps.Marker({\n" +
                "position: new google.maps.LatLng(" + latitude + ", " + longitude + "),\n" +
                "map: mapWorld,\n" +
                "title:\"" + name + "\",\n" +
                "icon: image\n" +
            "});\n" +
            "google.maps.event.addListener(marker" + id + ", 'click', function() {\n" + 
                "infowindow" + id + ".open(mapWorld,marker" + id + ");\n" +
            "});\n"
        ;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
