package petfinder.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import petfinder.bean.UserBean;
import petfinder.db.UserManagement;
import petfinder.exception.PetFinderException;

public class UserFormServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserBean newUser = new UserBean();
        newUser.setId(Integer.parseInt(request.getParameter("userId")));
        newUser.setName(request.getParameter("username"));
        newUser.setMailAddress(request.getParameter("mail"));
        newUser.setPassword(request.getParameter("newPassword"));
        try {
            UserManagement.updateUser(newUser);
            HttpSession session = request.getSession();
            newUser.setPassword(null);
            newUser.setIsLoggedIn(true);
            session.setAttribute("currentSessionUser", newUser);
        } catch (PetFinderException ex) {
            sendErrorRedirect(request, response, ex);
        }
        response.sendRedirect("Dashboard.jsp");
    }

    protected void sendErrorRedirect(HttpServletRequest request,
            HttpServletResponse response, PetFinderException pfex)
            throws ServletException, IOException {
        request.setAttribute("javax.servlet.jsp.jspException", pfex);
        getServletConfig().getServletContext().
                getRequestDispatcher("/simpleErrorPage.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
