/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package petfinder.shared;

/**
 *
 * @author Ole
 */
public class GeoData {
    private String type;
    private double latitude;
    private double longitude;
    private String SensorName;
    private String name;
    private long timestamp;
    private String othername;
    private double otherlatitude;
    private double otherlongitude;



    public GeoData() {
    }

    
// <editor-fold defaultstate="collapsed" desc="getter-setter">
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getSensorName() {
        return SensorName;
    }

    public void setSensorName(String SensorName) {
        this.SensorName = SensorName;
    }

    public String getOthername() {
        return othername;
    }

    public void setOthername(String othername) {
        this.othername = othername;
    }

    public double getOtherlatitude() {
        return otherlatitude;
    }

    public void setOtherlatitude(double otherlatitude) {
        this.otherlatitude = otherlatitude;
    }

    public double getOtherlongitude() {
        return otherlongitude;
    }

    public void setOtherlongitude(double otherlongitude) {
        this.otherlongitude = otherlongitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
    // </editor-fold>
}
