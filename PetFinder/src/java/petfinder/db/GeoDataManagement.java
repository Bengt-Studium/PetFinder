package petfinder.db;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import petfinder.bean.GeoDataBean;
import static petfinder.db.Common.printSQLException;

public class GeoDataManagement {

    public static ArrayList<GeoDataBean> getGeoData() {
        ArrayList<GeoDataBean> geoDataSet = new ArrayList<GeoDataBean>();
//        String query = "select * "
//                   + "from locations as la "
//                   + "where timestamp="
//                   + "(select max(timestamp) "
//                   + "from locations as lb "
//                   + "where la.animalID=lb.animalID)";
        String query = "select * from locations";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()) {
                GeoDataBean geoData = new GeoDataBean();
                geoData.setLongitude(resultSet.getFloat("longitude"));
                geoData.setLatitude(resultSet.getFloat("latitude"));
                geoData.setAnimalID(resultSet.getInt("animalID"));
                geoData.setTime(resultSet.getTimestamp("timestamp"));
                geoDataSet.add(geoData);
            }
            geoDataSet.trimToSize();
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQL Exception in GeoDataManagement.getGeoData");
            printSQLException(sqlex);
            geoDataSet = null;
        }

        return geoDataSet;
    }

    public static void insertGeoData(GeoDataBean geoData) {
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            boolean hasEntry = hasEntry(geoData.getAnimalID());
            String sql;
            if (hasEntry) {
                sql = "update locations set "
                        + "timestamp=?, "
                        + "longitude=?, "
                        + "latitude=? "
                        + "where animalId=?";
            } else {
                sql = "insert into locations values(DEFAULT, ?, ?, ?, ?)";
            }
            PreparedStatement prepStatement = connection.prepareStatement(sql);
            if (hasEntry) {
                prepStatement.setTimestamp(1, geoData.getTime());
                prepStatement.setFloat(2, geoData.getLongitude());
                prepStatement.setFloat(3, geoData.getLatitude());
                prepStatement.setInt(4, geoData.getAnimalID());
            } else {
                prepStatement.setInt(1, geoData.getAnimalID());
                prepStatement.setTimestamp(2, geoData.getTime());
                prepStatement.setFloat(3, geoData.getLongitude());
                prepStatement.setFloat(4, geoData.getLatitude());
            }
            prepStatement.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            System.err.println("SQL Exception in GeoDataManagement.insertGeoData");
            printSQLException(ex);
        }
    }

    private static boolean hasEntry(int animalID) {
        boolean returnValue = false;
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            String query = "select count(*) from locations where animalID=?";
            PreparedStatement prepStmt = connection.prepareStatement(query);
            prepStmt.setInt(1, animalID);
            ResultSet results = prepStmt.executeQuery();
            returnValue = results.next();
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQL Exception in GeoDataManagement.hasEntry");
            printSQLException(sqlex);
        }
        return returnValue;
    }
}
