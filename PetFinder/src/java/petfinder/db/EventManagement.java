package petfinder.db;

import java.sql.PreparedStatement;
import java.util.Date;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import static  java.sql.Types.*;
import static petfinder.db.Common.*;

public class EventManagement {
    public static void insertWallPost(int from, int to, String content) {
        //System.out.println("\nINFO: inserting wall post; content: " + content + "\n");
        insertEvent((short)0, new Timestamp(new Date().getTime()), null, null,
                from, to, content);
    }

    public static void insertFriendship(int actor1, int actor2) {
        Timestamp time = new Timestamp(new Date().getTime());
        insertEvent((short)1, time, null, null, actor1, actor2, null);
        insertEvent((short)1, time, null, null, actor2, actor1, null);
    }

    public static void insertMeeting(Timestamp time, float longitude,
            float latitude, int actor1, int actor2) {
        boolean insertNew = true;
        Connection connection = ConnectionManager.getInstance().getConnection();
        String query = "select id from events "
                + "where type=3 and "
                + "{fn TIMESTAMPDIFF(SQL_TSI_MINUTE, timestamp, CURRENT_TIMESTAMP)} < 5 and "
                + "actor1=? and "
                + "actor2=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setInt(1, actor1);
            stmt.setInt(2, actor2);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                insertNew = false;
            }
            connection.commit();
        } catch (SQLException ex) {
            System.err.println("SQL Exception in EventManagement.insertMeeting");
            printSQLException(ex);
        }

        if (insertNew) { 
            insertEvent((short)3, time, longitude, latitude, actor1, actor2, null);
            insertEvent((short)3, time, longitude, latitude, actor2, actor1, null);
        }
    }

    private static void insertEvent(Short type, Timestamp time, Float longitude,
            Float latitude, Integer actor1, Integer actor2, String content) {
        if (null == type)
            throw new IllegalArgumentException("type must not be null");
        if (null == time)
            throw new IllegalArgumentException("time must not be null");
        if (null == actor1)
            throw new IllegalArgumentException("actor1 must not be null");
        if (null == actor2)
            throw new IllegalArgumentException("actor2 must not be null");
        if (null != content && 1000 < content.length()) {
            throw new IllegalArgumentException("content must be at most 1000 characters long");
        }

        Connection connection = ConnectionManager.getInstance().getConnection();
        String insert = "insert into events values(DEFAULT, ?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement insertion = connection.prepareStatement(insert);
            insertion.setShort(1, type);
            insertion.setTimestamp(2, time);
            if (null == longitude)
                insertion.setNull(3, FLOAT);
            else
                insertion.setFloat(3, longitude);
            if (null == latitude)
                insertion.setNull(4, FLOAT);
            else
                insertion.setFloat(4, latitude);
            insertion.setInt(5, actor1);
            insertion.setInt(6, actor2);
            if (null == content)
                insertion.setNull(7, VARCHAR);
            else
                insertion.setString(7, content);
            int n = insertion.executeUpdate();

            connection.commit();
        } catch (SQLException e) {
            System.err.println("SQL Exception in EventManagement.insertEvent");
            printSQLException(e);
        }
    }
}
