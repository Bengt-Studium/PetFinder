package petfinder.db;
import static petfinder.db.Common.printSQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import petfinder.bean.AnimalBean;
import petfinder.bean.UserBean;
import petfinder.exception.PetFinderException;

public class UserManagement {

    public static UserBean login(UserBean userBean) {
        // TODO throw Exception containing details (like "username already taken")
        String userName = userBean.getName();
        String password = userBean.getPassword();
        String query = "select * from users where name=? and password=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(query);
            prepStmt.setString(1, userName);
            prepStmt.setInt(2, password.hashCode());
            ResultSet resultSet = prepStmt.executeQuery();
            if (!resultSet.next()) {
                userBean.setIsLoggedIn(false);
            } else {
                userBean.setId(resultSet.getInt("id"));
                userBean.setMailAddress(resultSet.getString("mailAdress"));
                userBean.setIsLoggedIn(true);
                prepStmt.close();
                
                String update = "update users set isLoggedIn="
                              + (userBean.getIsLoggedIn()?"1":"0")
                              + " where id="
                              + userBean.getId();
                prepStmt = connection.prepareStatement(update);
                if (1 != prepStmt.executeUpdate()) {
                    System.err.println("Unknown Error in UserManagement.login()."
                            + "If no Exception occured, it's probably OK anyway.");
                }
            }
            
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQL Exception in UserManagement.login");
            printSQLException(sqlex);
            userBean = null;
        }

        return userBean;
    }

    public static UserBean logout(UserBean userBean) {
        String userName = userBean.getName();
        String password = userBean.getPassword();
        Connection connection = ConnectionManager.getInstance().getConnection();
        String update = "update users set isLoggedIn=0 where name=?";
        try {
            PreparedStatement prepStmt = connection.prepareStatement(update);
            prepStmt.setString(1, userBean.getName());
            prepStmt.executeUpdate();
            connection.commit();
            userBean.setIsLoggedIn(false);
        } catch (SQLException sqlex) {
            System.err.println("SQL Exception in UserManagement.logout");
            printSQLException(sqlex);
            userBean = null;
        }

        return userBean;
    }

    public static UserBean register(UserBean newUser) {
        if (newUser == null || newUser.getName() == null || newUser.getMailAddress() == null
                || newUser.getPassword() == null || newUser.getName().equals("")
                || newUser.getMailAddress().equals("") || newUser.getPassword().equals("")) {
            return null;
        }

        String query = "select * from users where name=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(query);
            prepStmt.setString(1, newUser.getName());
            ResultSet resultSet = prepStmt.executeQuery();
            if (!resultSet.next()) {
                prepStmt.close();
                String insertion = "insert into users values(DEFAULT, ?, ?, ?, 0)";
                prepStmt = connection.prepareStatement(insertion);
                prepStmt.setString(1, newUser.getName());
                prepStmt.setInt(2, newUser.getPassword().hashCode());
                prepStmt.setString(3, newUser.getMailAddress());
                if (1 == prepStmt.executeUpdate()) {
                    newUser.setIsLoggedIn(true);
                } else {
                    System.err.println("Failed to insert the User into the DB. "
                        + "There probably is an Exception for you to look at.");
                    newUser = null;
                }
            } else {
                newUser = null;
            }

            connection.commit();
        } catch(SQLException sqlex) {
            System.err.println("SQL Exception in UserManagement.register");
            printSQLException(sqlex);
        }

        return newUser;
    }

    public static void updateUser(UserBean newUser) throws PetFinderException {
        if (newUser == null)
            throw new IllegalArgumentException("animal was null");
        UserBean oldUser = getUser(newUser.getId());
        if (oldUser == null)
            throw new IllegalArgumentException("user with id " + newUser.getId() + "does not exist");
        if (newUser.getName() != null && !newUser.getName().equals(oldUser.getName()))
            updateUserName(newUser.getId(), newUser.getName());
        if (newUser.getPassword() != null && !newUser.getPassword().equals(""))
            updateUserPassword(newUser.getId(), newUser.getPassword().hashCode());
        if (newUser.getMailAddress() != null && !newUser.getMailAddress().equals(oldUser.getMailAddress()))
            updateUserMailAddress(newUser.getId(), newUser.getMailAddress());
    }

    private static void updateUserName(int id, String name) {
        String update = "update users set name=? where id=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(update);
            prepStmt.setString(1, name);
            prepStmt.setInt(2, id);
            prepStmt.executeUpdate();
            connection.commit();
        } catch(SQLException sqlex) {
            System.err.println("SQLException in UserManagement.updateUserName");
            printSQLException(sqlex);
        }
    }

    private static void updateUserPassword(int id, int passwordHash) {
        String update = "update users set password=? where id=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(update);
            prepStmt.setInt(1, passwordHash);
            prepStmt.setInt(2, id);
            prepStmt.executeUpdate();
            connection.commit();
        } catch(SQLException sqlex) {
            System.err.println("SQLException in UserManagement.updateUserPassword");
            printSQLException(sqlex);
        }
    }

    private static void updateUserMailAddress(int id, String mail) {
        String update = "update users set mailAdress=? where id=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(update);
            prepStmt.setString(1, mail);
            prepStmt.setInt(2, id);
            prepStmt.executeUpdate();
            connection.commit();
        } catch(SQLException sqlex) {
            System.err.println("SQLException in UserManagement.updateUserMailAddress");
            printSQLException(sqlex);
        }
    }

    private static ArrayList<Integer> getAnimalIDs(int userID) {
        ArrayList<Integer> aids = new ArrayList<Integer>();
        String query = "select aid from user_animal where uid=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(query);
            prepStmt.setInt(1, userID);
            ResultSet resultSet = prepStmt.executeQuery();
            while(resultSet.next()) {
                aids.add(resultSet.getInt("aid"));
            }
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQLException in UserManagement.getAnimalIDs");
            printSQLException(sqlex);
            aids = null;
        }
        return aids;
    }

    public static ArrayList<AnimalBean> getAnimals(int userID) {
        ArrayList<Integer> aids = getAnimalIDs(userID);
        ArrayList<AnimalBean> animals = new ArrayList<AnimalBean>();
        String animalQuery = "select * from animals where id=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(animalQuery);
            int aid;
            ResultSet animalResults;
            AnimalBean animal = null;
            for (int i = 0; i < aids.size(); i++) {
                aid = aids.get(i);
                prepStmt.setInt(1, aid);
                prepStmt.execute();
                animalResults = prepStmt.getResultSet();
                if (animalResults.next()) {
                    animal = new AnimalBean();
                    animal.setId(aid);
                    animal.setName(animalResults.getString("name"));
                    animal.setImageURL(animalResults.getString("pictureURL"));
                    animal.setPlace(animalResults.getString("place"));
                    animals.add(animal);
                } else {
                    System.err.println("UserManagement.getAnimals: WARNING: Got an empty animal.");
                }
            }

            connection.commit();
        } catch(SQLException sqlex) {
            System.err.println("SQL Exception in UserManagement.getAnimals");
            printSQLException(sqlex);
            animals = null;
        }
        
        animals.trimToSize();
        return animals;
    }

    public static UserBean getUser(int id) {
        String query = "select * from users where id=?";
        UserBean user = null;
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(query);
            prepStmt.setInt(1, id);
            ResultSet resultSet = prepStmt.executeQuery();
            if (resultSet.next()) {
                user = new UserBean();
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("name"));
                user.setMailAddress("mailAdress");
            }
            connection.commit();
        } catch(SQLException sqlex) {
            System.err.println("SQL Exception in UserManagement.getUser");
            printSQLException(sqlex);
            user = null;
        }

        return user;
    }

    public static boolean userExists(int id) {
        String query = "select * from users where id=?";
        boolean userExists = false;
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(query);
            prepStmt.setInt(1, id);
            ResultSet resultSet = prepStmt.executeQuery();
            if (resultSet.next()) {
                userExists = true;
            }
            connection.commit();
        } catch(SQLException sqlex) {
            System.err.println("SQL Exception in UserManagement.getUser");
            printSQLException(sqlex);
        }

        return userExists;
    }
}
