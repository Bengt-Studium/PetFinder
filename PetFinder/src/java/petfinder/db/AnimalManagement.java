package petfinder.db;

import petfinder.exception.PetFinderException;
import java.util.Date;
import java.sql.Timestamp;
import petfinder.bean.GeoDataBean;
import petfinder.bean.UserBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import petfinder.bean.AnimalBean;
import petfinder.bean.EventBean;
import static petfinder.db.Common.*;

public class AnimalManagement {
    public static final int MAX_EVENTS = 5;

    public static boolean createNewAnimal(int userID, AnimalBean newAnimal) throws PetFinderException {
        boolean success = false;
        if (newAnimal.getName().length() > 40) {
            throw new IllegalArgumentException("name must be shorter than 40 characters");
        }
        if (newAnimal.getImageURL().length() > 300) {
            throw new IllegalArgumentException("picture url has wrong format");
        }
        if (newAnimal.getPlace().length() > 50) {
            throw new IllegalArgumentException("place must be shorter than 50 charactest");
        }
        if (!UserManagement.userExists(userID))
            throw new IllegalArgumentException("User does not exist");
        String insert = "insert into animals values(DEFAULT, ?, ?, ?)";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(insert);
            prepStmt.setString(1, newAnimal.getName());
            prepStmt.setString(2, newAnimal.getImageURL());
            prepStmt.setString(3, newAnimal.getPlace());
            prepStmt.executeUpdate();
            success = true;
        } catch(SQLException slqex) {
            System.err.println("SQL Exception in AnimalManagement.createNewAnimal");
            printSQLException(slqex);
            throw new PetFinderException("Failed to create new Animal(1)");
        }

        connection = ConnectionManager.getInstance().getConnection();
        String select = "select id from animals where id=(select max(id) from animals)";
        try {
            Statement stmt = connection.createStatement();
            ResultSet results = stmt.executeQuery(select);
            if (results.next()) {
                newAnimal.setId(results.getInt("id"));
            }
            success = true;
            connection.commit();
        } catch(SQLException slqex) {
            System.err.println("SQL Exception in AnimalManagement.createNewAnimal");
            printSQLException(slqex);
            throw new PetFinderException("Failed to create new Animal(2)");
        }

        connection = ConnectionManager.getInstance().getConnection();
        insert = "insert into user_animal values(?, ?)";
        try {
            PreparedStatement prepStmt = connection.prepareStatement(insert);
            prepStmt.setInt(1, userID);
            prepStmt.setInt(2, newAnimal.getId());
            prepStmt.executeUpdate();
            success = true;
        } catch(SQLException slqex) {
            System.err.println("SQL Exception in AnimalManagement.createNewAnimal");
            printSQLException(slqex);
            throw new PetFinderException("Failed to create new Animal(3)");
        }
        return success;
    }

    public static void updateAnimal(AnimalBean newAnimal) {
        if (newAnimal == null)
            throw new IllegalArgumentException("animal was null");
        AnimalBean oldAnimal = getAnimal(newAnimal.getId());
        if (oldAnimal == null)
            throw new IllegalArgumentException("animal with id " + newAnimal.getId() + "does not exist");
        String update = "update animals set name=?, pictureURL=?, place=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(update);
            if (newAnimal.getName() != null)
                prepStmt.setString(1, newAnimal.getName());
            else
                prepStmt.setString(1, oldAnimal.getName());

            if (newAnimal.getImageURL() != null)
                prepStmt.setString(2, newAnimal.getImageURL());
            else
                prepStmt.setString(2, oldAnimal.getImageURL());
            
            if (newAnimal.getPlace() != null)
                prepStmt.setString(3, newAnimal.getPlace());
            else
                prepStmt.setString(3, oldAnimal.getPlace());
            prepStmt.executeUpdate();
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQLException in AnimalManagement.updateAnimal");
            printSQLException(sqlex);
        }
    }

    public static AnimalBean getAnimal(int id) {
        AnimalBean animal = null;
        String query = "select * from animals where id=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(query);
            prepStmt.setInt(1, id);
            ResultSet resultSet = prepStmt.executeQuery();
            if (resultSet.next()) {
                animal = new AnimalBean();
                animal.setId(id);
                animal.setName(resultSet.getString("name"));
                animal.setImageURL(resultSet.getString("pictureURL"));
                animal.setPlace(resultSet.getString("place"));
            }
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQL Exception in AnimalManagement.getAnimal");
            printSQLException(sqlex);
            animal = null;
        }
        return animal;
    }

    public static ArrayList<String> getOwners(int id) {
        ArrayList<String> owners = new ArrayList<String>();
        String query = "select uid from user_animal where aid=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(query);
            prepStmt.setInt(1, id);
            ResultSet resultSet = prepStmt.executeQuery();
            while(resultSet.next()) {
                UserBean owner = UserManagement.getUser(resultSet.getInt("uid"));
                if (owner != null) {
                    owners.add(owner.getName());
                } else {
                    System.err.println("AnimalManagement.getOwners unexptectedly received a null owner/user.");
                }
            }
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQL Exception in AnimalManagement.getOwners");
            printSQLException(sqlex);
            owners = null;
        }
        owners.trimToSize();
        return owners;
    }

    public static ArrayList<EventBean> getEvents(int id) {
        return getEventsBefore(id, new Timestamp(new Date().getTime()));
    }

    public static ArrayList<EventBean> getEventsBefore(int id, Timestamp time) {
        ArrayList<EventBean> events = new ArrayList<EventBean>();
        String query = "select * from events where actor2=? AND timestamp < ? order by timestamp desc";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStatement =  connection.prepareStatement(query);
            prepStatement.setInt(1, id);
            prepStatement.setTimestamp(2, time);
            ResultSet resultSet = prepStatement.executeQuery();
            EventBean event;
            for (int i = 0; i < MAX_EVENTS && resultSet.next(); i++) {
                event = new EventBean();
                event.setId(resultSet.getInt("id"));
                event.setType(resultSet.getShort("type"));
                event.setTimestamp(resultSet.getTimestamp("timestamp"));
                event.setLongitude(resultSet.getFloat("longitude"));
                event.setLatitude(resultSet.getFloat("latitude"));
                event.setActor1(resultSet.getInt("actor1"));
                event.setActor2(resultSet.getInt("actor2"));
                event.setContent(resultSet.getString("content"));
                events.add(event);
            }
            if (!resultSet.next())
                events.add(null);
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQL Exception in AnimalManagement.getEventsBefore");
            printSQLException(sqlex);
            events = null;
        }

        events.trimToSize();
        return events;
    }

    public static ArrayList<EventBean> getEventsAfter(int id, Timestamp time) {
        ArrayList<EventBean> events = new ArrayList<EventBean>();
        String query = "select * from events where actor2=? AND timestamp > ? order by timestamp asc";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStatement =  connection.prepareStatement(query);
            prepStatement.setInt(1, id);
            prepStatement.setTimestamp(2, time);
            ResultSet resultSet = prepStatement.executeQuery();
            EventBean event;
            for (int i = 0; resultSet.next(); i++) {
                event = new EventBean();
                event.setId(resultSet.getInt("id"));
                event.setType(resultSet.getShort("type"));
                event.setTimestamp(resultSet.getTimestamp("timestamp"));
                event.setLongitude(resultSet.getFloat("longitude"));
                event.setLatitude(resultSet.getFloat("latitude"));
                event.setActor1(resultSet.getInt("actor1"));
                event.setActor2(resultSet.getInt("actor2"));
                event.setContent(resultSet.getString("content"));
                events.add(event);
            }
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQL Exception in AnimalManagement.getEventsAfter");
            printSQLException(sqlex);
            events = null;
        }

        events.trimToSize();
        return events;
    }

    public static int getEventCountBefore(int id, Timestamp time) {
        String query = "select count(*) c from events where actor2=? AND timestamp > ?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        int count = 0;
        try {
            PreparedStatement prepStatement = connection.prepareStatement(query);
            prepStatement.setInt(1, id);
            prepStatement.setTimestamp(2, time);
            ResultSet results = prepStatement.executeQuery();
            if (results.next()) {
                count = results.getInt("c");
            }
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQL Exception in AnimalManagement.getEventCountBefore");
            printSQLException(sqlex);
        }
        return count;
    }

    public static ArrayList<AnimalBean> getFriendList(int id) {
        ArrayList<AnimalBean> friendList = new ArrayList<AnimalBean>();
        String query = "select aid,fid from animal_friend where aid=? OR fid=?";
              //+ " AND acceptance=1"; // we currently do not use acceptance
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(query);
            prepStmt.setInt(1, id);
            prepStmt.setInt(2, id);
            ResultSet resultSet = prepStmt.executeQuery();
            AnimalBean friend = null;
            while(resultSet.next()) {
                int fid = resultSet.getInt("fid");
                int aid = resultSet.getInt("aid");
                if (fid == id) {
                    friend = getAnimal(aid);
                } else {
                    friend = getAnimal(fid);
                }
                if (friend != null) {
                    friendList.add(friend);
                } else {
                    System.err.println("AnimalManagement: Unable to retrieve friend, check for errors.");
                }
            }
            connection.commit();
        } catch (SQLException sqlex) {
            System.err.println("SQL Exception in AnimalManagement.getFriendList");
            printSQLException(sqlex);
            // TODO throw exception
            friendList = null;
        }
        friendList.trimToSize();
        return friendList;
    }

    public static ArrayList<AnimalBean> getSocialGraph(int viewer, int viewee) {
        AnimalBean viewerAnimal = getAnimal(viewer);
        ArrayList<AnimalBean> friendsOfViewee = null;
        ArrayList<AnimalBean> friendsOfFriendsOfViewee = null;
        ArrayList<AnimalBean> friendsOfFriendsOfViewer = null;
        ArrayList<AnimalBean> friendsOfViewer = null;
        ArrayList<AnimalBean> socialGraph = new ArrayList<AnimalBean>();
        friendsOfViewee = getFriendList(viewee);
        if (viewer == viewee) {
            socialGraph.add(viewerAnimal);
            socialGraph.trimToSize();
            return socialGraph;
        }

        boolean done = false;
        for (int i = 0; !done && i < friendsOfViewee.size(); i++) {
            if (friendsOfViewee.get(i).getId() == viewer) {
                socialGraph.add(viewerAnimal);
                done = true;
            }
        }

        
        if (!done) {
            friendsOfViewer = getFriendList(viewer);
            for (int i = 0; !done && i < friendsOfViewee.size(); i++) {
                for (int j = 0; !done && j < friendsOfViewer.size(); j++) {
                    if (friendsOfViewee.get(i).getId() == friendsOfViewer.get(j).getId()) {
                        socialGraph.add(friendsOfViewee.get(i));
                        socialGraph.add(viewerAnimal);
                        done = true;
                    }
                }
            }
        }

        
        if (!done) {
            for (int i = 0; !done && i < friendsOfViewee.size(); i++) {
                friendsOfFriendsOfViewee = getFriendList(friendsOfViewee.get(i).getId());
                for (int j = 0; !done && j < friendsOfFriendsOfViewee.size(); j++) {
                    for (int k = 0; !done && k < friendsOfViewer.size(); k++) {
                        if (friendsOfViewer.get(k).getId() == friendsOfFriendsOfViewee.get(j).getId()) {
                            socialGraph.add(friendsOfViewee.get(i));
                            socialGraph.add(friendsOfFriendsOfViewee.get(j));
                            socialGraph.add(viewerAnimal);
                            done = true;
                        }
                    }
                }
            }
        }

        if (!done) {
            for (int i = 0; !done && i < friendsOfViewee.size(); i++) {
                friendsOfFriendsOfViewee = getFriendList(friendsOfViewee.get(i).getId());
                for (int j = 0; !done && j < friendsOfFriendsOfViewee.size(); j++) {
                    for (int k = 0; !done && k < friendsOfViewer.size(); k++) {
                        friendsOfFriendsOfViewer = getFriendList(friendsOfViewer.get(k).getId());
                        for (int l = 0; !done && l < friendsOfFriendsOfViewer.size(); l++) {
                            if (friendsOfFriendsOfViewer.get(l).getId() == friendsOfFriendsOfViewee.get(j).getId()) {
                                socialGraph.add(friendsOfViewee.get(i));
                                socialGraph.add(friendsOfFriendsOfViewer.get(l));
                                socialGraph.add(friendsOfViewer.get(k));
                                socialGraph.add(viewerAnimal);
                                done = true;
                            }
                        }
                    }
                }
            }
        }

        socialGraph.trimToSize();
        return socialGraph;
    }

    public static GeoDataBean getCurrentPosition(int id) {
        GeoDataBean geoData = null;
//        String select = "select * from locations as la where animalID=" + id
//                + " AND timestamp=(select max(timestamp) "
//                + "from locations as lb "
//                + "where la.animalID=lb.animalID)";
        String select = "select * from locations where animalID=?";
        Connection connection = ConnectionManager.getInstance().getConnection();
        try {
            PreparedStatement prepStmt = connection.prepareStatement(select);
            prepStmt.setInt(1, id);
            ResultSet results = prepStmt.executeQuery();
            if (results.next()) {
                geoData = new GeoDataBean();
                geoData.setAnimalID(id);
                geoData.setLatitude(results.getFloat("latitude"));
                geoData.setLongitude(results.getFloat("longitude"));
                geoData.setTime(results.getTimestamp("timestamp"));
            }
        } catch (SQLException ex) {
            System.err.println("SQL Exception in AnimalManagement.getCurrentPosition");
            printSQLException(ex);
            geoData = null;
        }
        return geoData;
    }
}
