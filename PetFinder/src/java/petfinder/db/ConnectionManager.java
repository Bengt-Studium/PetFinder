package petfinder.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import static petfinder.db.Common.printSQLException;

public class ConnectionManager {
    private static final ConnectionManager instance = new ConnectionManager();
    // private static String framework = "embedded";
    private final String driver = "org.apache.derby.jdbc.EmbeddedDriver";
    private final String protocol = "jdbc:derby:";
    private final String dbName = this.getDBPath();
    private Connection connection;

    private ConnectionManager() {
        loadDriver();
        try {
            this.connection = DriverManager.getConnection(protocol + dbName);
            this.connection.setAutoCommit(false);
        } catch (SQLException ex) {
            System.err.println("SQL Exception in ConnectionManager contructor");
            printSQLException(ex);
        }
    }

    private String getDBPath() {
        String userName = System.getProperty("user.name");
        if (userName.equals("Ole")) {
            return "C:\\PetDB";
        } else if (userName.equals("bengt")) {
            return "/home/bengt/Studium/git/1011/it/Vorstellungen/PetFinder I/Prototyp/db";
        } else if (userName.equals("kto") || userName.equals("it11")) {
            return "/opt/DB/PetDB";
        } else  {
            return "/var/DB/PetDB";
        }
    }

    public static ConnectionManager getInstance() {
        return instance;
    }

    public Connection getConnection() {
        return this.connection;
    }

    private void loadDriver() {
        try {
            Class.forName(driver).newInstance();
        } catch (ClassNotFoundException cnfe) {
            System.err.println("\nUnable to load the JDBC driver " + driver);
            System.err.println("Please check your CLASSPATH.");
            cnfe.printStackTrace(System.err);
        } catch (InstantiationException ie) {
            System.err.println(
                        "\nUnable to instantiate the JDBC driver " + driver);
            ie.printStackTrace(System.err);
        } catch (IllegalAccessException iae) {
            System.err.println(
                        "\nNot allowed to access the JDBC driver " + driver);
            iae.printStackTrace(System.err);
        }
    }
}
