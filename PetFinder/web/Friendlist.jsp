<%@page contentType="text/html" pageEncoding="UTF-8"
        import="petfinder.bean.UserBean"
        import="petfinder.bean.AnimalBean"
        import="petfinder.db.AnimalManagement"
        import="petfinder.exception.PetFinderException"
        import="java.util.ArrayList"
        errorPage="simpleErrorPage.jsp" %>
<%
    UserBean currentUser = (UserBean)session.getAttribute("currentSessionUser");
    if (currentUser == null || !currentUser.getIsLoggedIn()) {
        throw new PetFinderException("You have to be logged in to view an animal's profile.");
    }

    AnimalBean animal;
    try {
        animal = AnimalManagement.getAnimal(Integer.parseInt(request.getParameter("id")));
    } catch (NumberFormatException nfe) {
        throw new PetFinderException("You requested an invalid animal ID.");
    }
    if (animal == null) {
        throw new PetFinderException("The requested animal does not exist.");
    }

    ArrayList<AnimalBean> friends = AnimalManagement.getFriendList(animal.getId());
%>
<div id="friendslist">
<%  for (int i = 0; i < friends.size(); i++) { %>
    <a href="Profile.jsp?id=<%= friends.get(i).getId() %>">
        <img src="<%= friends.get(i).getImageURL() %>"/>
    </a>
<%  } %>
</div>