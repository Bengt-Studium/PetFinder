<%@page import="java.sql.Timestamp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"
        import="petfinder.bean.AnimalBean"
        import="petfinder.bean.EventBean"
        import="petfinder.bean.UserBean"
        import="petfinder.db.AnimalManagement"
        import="petfinder.db.UserManagement"
        import="petfinder.exception.PetFinderException"
        import="java.util.ArrayList" %>
<%
    // Ensure the user is logged in
    UserBean currentUser = (UserBean)session.getAttribute("currentSessionUser");
    if (currentUser == null || !currentUser.getIsLoggedIn()) {
        throw new PetFinderException("You have to be logged in to view an animal's profile.");
    }

    // Ensure we have an animal
    AnimalBean animal;
    try {
        animal = AnimalManagement.getAnimal(Integer.parseInt(request.getParameter("id")));
    } catch (NumberFormatException nfe) {
        throw new PetFinderException("You requested an invalid animal ID.");
    }
    if (animal == null) {
        throw new PetFinderException("The requested animal does not exist.");
    }

    // Get events starting with last known timestamp or from the start
    Timestamp oldestEvent = (Timestamp) request.getSession().getAttribute("oldestEvent");

    // Get the next list of events
    ArrayList<EventBean> events = null;
    switch (Integer.parseInt(request.getParameter("type"))) {
        case 0:
            /*older*/
            if (oldestEvent == null) {
                /* we initialize the EventStream */
                events = AnimalManagement.getEvents(animal.getId());
                if (events == null) {
                    throw new PetFinderException(
                        "Unable to receive the event stream for this profile page.");
                }


                /* keep the session current */
                if (events.get(events.size() - 1) == null && events.size() >= 2) {
                    /* the first call already contains the last event */
                    request.getSession().setAttribute("oldestEvent", events.get(events.size() - 2).getTimestamp());
                    request.getSession().setAttribute("latestEvent", events.get(0).getTimestamp());
                } else
                    if (events.get(events.size() - 1) == null) {
                        /* The event stream is empty */
                        request.getSession().setAttribute("oldestEvent", new Timestamp(0));
                        request.getSession().setAttribute("latestEvent", new Timestamp(0));
                    } else {
                        /* The event stream is contains 5 events */
                        request.getSession().setAttribute("oldestEvent", events.get(events.size() - 1).getTimestamp());
                        request.getSession().setAttribute("latestEvent", events.get(0).getTimestamp());
                    }

            } else {
                /* we extend the Eventstream downwards / to the past */
                events = AnimalManagement.getEventsBefore(animal.getId(), oldestEvent);
                if (events == null) {
                    throw new PetFinderException(
                        "Unable to receive the event stream for this profile page.");
                }

                /* keep the session current */
                        /* keep the session current */
                if (events.get(events.size() - 1) == null)
                    /* this call already contains the last event */
                    request.getSession().setAttribute("oldestEvent", events.get(events.size() -2).getTimestamp());
                else
                    /* this call does not contain the last event */
                    request.getSession().setAttribute("oldestEvent", events.get(events.size() -1).getTimestamp());
            }
            break;
        case 1:
            /* newer */
            events = AnimalManagement.getEventsAfter(animal.getId(), (Timestamp) request.getSession().getAttribute("latestEvent"));
            if (events == null) {
                throw new PetFinderException(
                    "Unable to receive the event stream for this profile page.");
            }
            if(events.size()>0) {
                request.getSession().setAttribute("latestEvent", (Timestamp) events.get(0).getTimestamp());
            }

            break;
    }

    // concatenate eventStream
    String eventStream = "";
    for (int i = 0; i < events.size(); i++) {
        if (events.get(i) == null) {
            if (request.getSession().getAttribute("eventStreamHasEnded").equals(false)) {
%>
<div class="grid_3 event">
    <img height="96px" width="96px" src="<%= animal.getImageURL() %>" />
    <img class="speechballoon" src="images/speechballoon.png" />
    <p>
        You have reached the end of my event stream.
    </p>
</div>
<% 
        }
            request.getSession().setAttribute("eventStreamHasEnded", true);

            break;
        } else {
            switch (events.get(i).getType()) {
            case 0:
                AnimalBean from = AnimalManagement.getAnimal(events.get(i).getActor1());
                if (from != null && from.getName() != null && from.getImageURL() != null) {
%>
<div class="grid_3 event">
    <a href="Profile.jsp?id=<%= from.getId() %>" >
        <img height="96px" width="96px" src="<%= from.getImageURL() %>" />
    </a>
    <img class="speechballoon" src="images/speechballoon.png" />
    <p>
        <%= events.get(i).getContent() %>
    </p>
</div>
<%
                }
                break;
            case 1:
                AnimalBean friend = AnimalManagement.getAnimal(
                        events.get(i).getActor1());
                if (friend == null) {
                    throw new PetFinderException("Did not find friend.");
                }
%>
<div class="grid_3 event">
    <a href="Profile.jsp?id=<%= friend.getId() %>">
        <img height="96px" width="96p" src="<%= friend.getImageURL() %>" />
    </a>
    <img class="speechballoon" src="images/speechballoon.png" />
    <p>
        I am now friends with <b><%= animal.getName() %></b>!
    </p>
</div>
<%
                break;
            case 2:
                UserBean custodian = UserManagement.getUser(events.get(i).getActor1());
                if (custodian != null && custodian.getName() != null) {
%>
<div class="grid_3 event">
    <img height="96px" width="96px" src="<%= animal.getImageURL() %>" />
    <img class="speechballoon" src="images/speechballoon.png" />
    <p>
        <%= custodian.getName() %> is now looking after me.
    </p>
</div>
<%
                }
                break;
            case 3:
                AnimalBean met = AnimalManagement.getAnimal(events.get(i).getActor1());
                if (met != null && met.getName() != null && met.getImageURL() != null) {
%>
<div class="grid_3 event">
    <a href="Profile.jsp?id=<%= met.getId() %>">
        <img height="96px" width="96px" src=" <%= met.getImageURL() %>" />
    </a>
    <img class="speechballoon" src="images/speechballoon.png" />
    <p>
        I met <b><%= animal.getName() %></b>!
    </p>
</div>
<%
                }
                break;
            }
        }
    }
%>
<%= eventStream %>