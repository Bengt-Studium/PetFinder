<%@page import="petfinder.exception.PetFinderException"%>
<%@page import="petfinder.bean.AnimalBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="petfinder.db.AnimalManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="simpleErrorPage.jsp" %>
<div id="socialGraph">
<%
int viewee;
try {
    viewee = Integer.parseInt(request.getParameter("viewee"));
} catch (NumberFormatException nfe) {
    throw new PetFinderException("Got an invalid parameter: " + request.getParameter("viewee"));
}
Integer viewer = (Integer)request.getSession().getAttribute("aID");
if (viewer != null) {
    ArrayList<AnimalBean> animals = AnimalManagement.getSocialGraph(viewer, viewee);
    for (int i = 0; i < animals.size(); i++) {
%>
   <div class="grid_1">
        <img src="images/arrow-left.png" />
        <a href="Profile.jsp?id=<%= animals.get(i).getId() %>">
            <img src="<%= animals.get(i).getImageURL() %>" heigth="128" width="128" />
        </a>
   </div>
<%
    }
} else {
%>
    You must select an animal to view the social graph.
<%
}
%>
</div>