<%@page contentType="text/html" pageEncoding="UTF-8"
        import="petfinder.bean.UserBean"
        import="petfinder.bean.AnimalBean"
        import="petfinder.db.UserManagement"
        import="petfinder.exception.PetFinderException"
        import="java.util.ArrayList"
        errorPage="simpleErrorPage.jsp" %>

<%
UserBean currentUser = (UserBean)session.getAttribute("currentSessionUser");
if (currentUser == null || !currentUser.getIsLoggedIn()) {
    throw new PetFinderException("You have to be logged in to view your dashboard.");
}

String breadcrumb = "&nbsp;>&nbsp;<a href=\"Dashboard.jsp\">" + currentUser.getName() + "</a>";
%>

<jsp:include page="index/beforeContent.jsp" flush="true">
    <jsp:param name="title" value="LandingPage" />
    <jsp:param name="breadcrumb" value="<%= breadcrumb %>" />
</jsp:include>

<%
ArrayList<AnimalBean> animals = UserManagement.getAnimals(currentUser.getId());
if (animals == null) {
    throw new PetFinderException("Dashboard: An error occured during a database request.");
}
%>

<div id="Dashboard" class="container_6 clearfix">
    <div class="container_6 clearfix">
        <div class="grid_3 event">
            <img src="images/favicon_96.png"/>
            <img class="speechballoon" src="images/speechballoon.png" />
            <p>
                Hello <%= currentUser.getName() %>,<br/>
                we know you as user #<%= currentUser.getId() %> and via <%= currentUser.getMailAddress() %>.<br/>
<%
if (animals.size() == 0) {
%>
                    There are no animals assigned to your account.
        </p>
   </div>
<%
} else {
%>
                    <br/>Choose one of your identities!
        </p>
    </div>
    <div class ="grid_3">
        <div class="half-identity">
            <h2>Update</h2>
                <jsp:include page="UserForm.jsp" flush="true" />
        </div>
    </div>
</div>

    <div class="container_6 clearfix">
<%
    for (int i = 0; i < animals.size(); i++) {
%>
        <div class="grid_1">
               <a href="Profile.jsp?id=<%= animals.get(i).getId() %>">
                    <div id="identity<%= i + 1 %>" class="identity">
                    <h2><%= animals.get(i).getName() %></h2>
                    <img class="dashboardPicture" src="<%= animals.get(i).getImageURL() %>" />
               </div>
            </a>
        </div>
<%
    }
}
%>
        <div class="grid_1">
            <div class="identity">
                <h2>add another</h2>
                <form class="identity-pseudo-img" action="CreatePetServlet" method="get" accept-charset="UTF-8">
                    <input type="hidden" name="userId" value="<%= currentUser.getId() %>" />
                    <input size=9 type="text" name="name" title="name" value="name"/><br/>
                    <input size=9 type="text" name="image" title="image-url" value="image"/><br/>
                    <input size=9 type="text" name="place" title="hometown" value="hometown"/><br/>
                    <input class="textfill" type="submit" value="send" />
                </form>
            </div>
        </div>
    </div>
</div>

<jsp:include page="index/afterContent.html" flush="true" />


<!-- TODO EventStreams -->