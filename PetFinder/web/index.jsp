<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="index/beforeContent.jsp" flush="true">
    <jsp:param name="title" value="LandingPage" />
    <jsp:param name="breadcrumb" value="" />
    <jsp:param name="worldMap" value="true" />
</jsp:include>

<div id="landingPage" class="container_6 clearfix">
    <div class="grid_6">
        <div id="worldview" style="width: 928px; height: 618px"></div>
    </div>
</div>

<jsp:include page="index/afterContent.html" flush="true" />