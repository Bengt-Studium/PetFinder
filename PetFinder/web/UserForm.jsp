<%-- 
    Document   : UserForm
    Created on : 09.02.2011, 04:46:14
    Author     : bengt
--%>

<%@page import="petfinder.exception.PetFinderException"%>
<%@page import="petfinder.bean.UserBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    UserBean currentUser = (UserBean)session.getAttribute("currentSessionUser");
    if (currentUser == null || !currentUser.getIsLoggedIn()) {
        throw new PetFinderException("You have to be logged in to view your profile.");
    }
%>
    <form action="UserFormServlet" method="post" accept-charset="UTF-8">
        <input type="hidden" name="userId" value="<%= currentUser.getId() %>" />
        <input class="noButton" size=12 type="text" name="username" title="username" value="<%= currentUser.getName() %>"/>
        <input class="noButton" size=24 type="text" name="mail" title="email address" value="<%= currentUser.getMailAddress() %>"/><br/>
        <input class="noButton" size=12 type="password" name="newPassword" title="Your new password; leave empty if you want it unchanged" value=""/>
        <input type="submit" value="update" />
    </form>
