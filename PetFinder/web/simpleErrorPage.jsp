<%-- 
    Document   : simpleErrorPage
    Created on : 16.01.2011, 17:29:28
    Author     : bengt
--%>

<%@ page isErrorPage="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="index/beforeContent.jsp" flush="true" >
    <jsp:param name="title" value="ErrorPage" />
    <jsp:param name="breadcrumb" value=""/>
</jsp:include>

            
<div id="errorPage" class="container_6 clearfix">
    <div class="grid_6">
        <h2>Something went wrong. See the error message for further details:</h2>
        
        <pre id="errorMessage"><%= exception.getMessage() %></pre>

        <center><img src="images/403.jpg"/></center>
    </div>
</div>

<jsp:include page="index/afterContent.html" flush="true" />