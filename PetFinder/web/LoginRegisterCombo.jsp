<%@ page pageEncoding="UTF-8" import="petfinder.bean.UserBean"%>

<%
UserBean currentUser = (UserBean)session.getAttribute("currentSessionUser");
if (currentUser == null || !currentUser.getIsLoggedIn()) {
%>

<div id="login" class="grid_3">
    <form action="LoginServlet" method="post">
        <input class="noButton" size=10 type="text" name="username" title="username" value="Username"/>
        <input class="noButton" size=10 type="password" name="password" title="password" value="Password"/>
        <input type="submit" value="Login" />
    </form>
    &nbsp;
    <a id="signMeUp" href="#!/register">Sign me up!</a>
</div>
<div id="register" class="grid_3" style="display:none;">
    <form action="RegistrationServlet" method="post">
        <input class="noButton" size=10 type="text" name="username" title="username" value="Username"/>
        <input class="noButton" size=10 type="text" name="mail" title="email address" value="Email"/>
        <input class="noButton" size=10 type="password" name="password" title="password" value="Password"/>
        <input type="submit" value="register" />
    </form>
</div>

<%  } else { %>

<div id="loggedIn" class="grid_3">
    <form action="LogoutServlet" method="get">
        <input type="submit" value="Logout" />
    </form>
    <span>Logged in as <a href="Dashboard.jsp"><%= currentUser.getName() %></a></span>
</div>

<%  } %>
