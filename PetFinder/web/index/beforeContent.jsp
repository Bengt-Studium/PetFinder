<%@page import="petfinder.db.AnimalManagement"%>
<%@page import="petfinder.bean.GeoDataBean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<head>

<title>PetFinder - <%= request.getParameter("title") %></title>
    
  <meta charset="utf-8">

  <meta http-equiv="content-type" content="text/html; charset=UTF-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta name="description" content="">
  <meta name="author" content="">

  <!--  Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="favicon.ico">
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon">
  <link rel="apple-touch-icon" href="favicon.png">


  <!-- CSS : implied media="all" -->
  <link rel="stylesheet" href="css/style.css?v=2">

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers
  <link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->

  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="js/libs/modernizr-1.6.min.js"></script>

  <script>
    <% if(request.getParameter("id") != null) {%>
    function updateEventStream() {
        $.ajax({
            url: "EventStream.jsp?type=1&id=<%= request.getParameter("id") %>",
            cache: false,
            success: function(html){
               $(html).hide().prependTo("#eventStream").slideDown("slow");
            }
        });
       t = setTimeout(function() {updateEventStream();}, 1000);
    }
   <%}%>
    </script>

  <script>
function getWindowHeight() {
    var windowHeight=0;
    if (typeof(window.innerHeight)=='number') {
        windowHeight=window.innerHeight;
    } else {
        if (document.documentElement && document.documentElement.clientHeight) {
            windowHeight= document.documentElement.clientHeight;
        } else {
            if (document.body&&document.body.clientHeight) {
                windowHeight=document.body.clientHeight;
            }
        }
    }
    return windowHeight - 32;
}

function setFooter() {
    if (document.getElementById) {
        var windowHeight = getWindowHeight();
        if (windowHeight>0) {
            var contentHeight = document.getElementById('content').offsetHeight;
            var footerElement = document.getElementById('footer');
            var footerHeight=footerElement.offsetHeight;
            if (windowHeight-(contentHeight+footerHeight)>=0) {
                footerElement.style.position='relative';
                footerElement.style.top=(windowHeight-
                (contentHeight+footerHeight))+'px';
            } else {
                footerElement.style.position = 'static';
            }
        }
    }
}
  </script>

  <!-- For some reason googlemaps stuff has to go up here -->
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
  <script type="text/javascript">
  var latit = 0;
  var longit = 0;
  var poly;
  var polyOptions = {
    strokeColor: '#00FF00',
    strokeOpacity: 0.7,
    strokeWeight: 3
  }
  poly = new google.maps.Polyline(polyOptions);
<% if(request.getParameter("profileMap")!=null && request.getParameter("profileMap").equals("true")) {%>
  function updatePos(marker) {
     $.get("PetPosLive?id=<%= request.getParameter("id")%>", function(data){
       var done = data.split(",");
       latit = done[0];
       longit = done[1];
     });
     if (typeof(latit) !== 'undefined' && latit != 0) {
         var pos = new google.maps.LatLng(latit, longit);
         marker.setPosition(pos);
        var path = poly.getPath();
      // Because path is an MVCArray, we can simply append a new coordinate
      // and it will automatically appear
         path.push(pos);
     }
   t=setTimeout(function() {updatePos(marker);},1000);
   }
<%
}
%>

  function initialize() {
    setFooter();

  <% if(request.getParameter("profileMap")!=null && request.getParameter("profileMap").equals("true")) {%>
  <% GeoDataBean centermap = AnimalManagement.getCurrentPosition(Integer.parseInt(request.getParameter("id")));
  if (centermap != null) {
%>
   var myOptions = {
        zoom: 18,
        center: new google.maps.LatLng(<%= centermap.getLatitude() %>, <%= centermap.getLongitude() %>),
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.HYBRID
    }
    var map = new google.maps.Map(document.getElementById("locate"), myOptions);
    poly.setMap(map);

    var image = new google.maps.MarkerImage('images/marker_16.png',
    new google.maps.Size(16, 16),
    new google.maps.Point(0,0),
    new google.maps.Point(8, 8)
    );
    <% String id = request.getParameter("id"); %>
    <jsp:include page="../PetGeoDataServlet" flush="true">
        <jsp:param name="id" value="<%= id %>" />
    </jsp:include>
    updatePos(marker<%= request.getParameter("id") %>);
    <%
   }
}%>

    <% if(request.getParameter("worldMap")!=null && request.getParameter("worldMap").equals("true")) {%>
    var myOptionsWorld = {
    zoom: 2,
    center: new google.maps.LatLng(50.580447, -9.550788),
    disableDefaultUI: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
    }
  var mapWorld = new google.maps.Map(document.getElementById("worldview"), myOptionsWorld);
  var image = 'images/marker_16.png';
    <jsp:include page="../GeoDataServlet" flush="true" />
    <%}%>

    <% if(request.getParameter("id") != null) {%>
    updateEventStream();
    <%}%>
  }
  </script>

</head>
<body onresize="setFooter()" onload="initialize()">

    <div id="content">
        <div class="poller"></div>
        <div id="header" class="container_6">
            <h1 id="breadcrumb" class="grid_3 ">
                <a href="index.jsp">PetFinder</a><%= request.getParameter("breadcrumb") %>
            </h1>
            <jsp:include page="../LoginRegisterCombo.jsp" flush="true" />
        </div>