<%@page contentType="text/html" pageEncoding="UTF-8"
        import="petfinder.bean.AnimalBean"
        import="petfinder.bean.EventBean"
        import="petfinder.bean.UserBean"
        import="petfinder.db.AnimalManagement"
        import="petfinder.db.UserManagement"
        import="petfinder.exception.PetFinderException"
        import="java.util.ArrayList"
        errorPage="simpleErrorPage.jsp" %>

<%
    // Set oldestEvent to 0 before calling the eventStream.jsp for the first time.
    request.getSession().setAttribute("oldestEvent", null);
    request.getSession().setAttribute("latestEvent", null);
    request.getSession().setAttribute("eventStreamHasEnded", false);

    UserBean currentUser = (UserBean)session.getAttribute("currentSessionUser");
    if (currentUser == null || !currentUser.getIsLoggedIn()) {
        throw new PetFinderException("You have to be logged in to view an animal's profile.");
    }
    if ((Integer)session.getAttribute("aID") == null) {
        throw new PetFinderException("You must have at least one animal to view an animal's profile.");
    }

    AnimalBean animal;
    try {
        animal = AnimalManagement.getAnimal(Integer.parseInt(request.getParameter("id")));
    } catch (NumberFormatException nfe) {
        throw new PetFinderException("You requested an invalid animal ID.");
    }

    if (animal == null) {
        throw new PetFinderException("The requested profile page does not exist.");
    }

    String breadcrumb = "&nbsp;>&nbsp;<a href=\"Dashboard.jsp\">" + currentUser.getName() + "</a>"
           + "&nbsp;>&nbsp;<a href=\"Profile.jsp?id=" + animal.getId() + "\">" + animal.getName() + "</a>";
%>

<jsp:include page="index/beforeContent.jsp" flush="true">
    <jsp:param name="title" value="<%= animal.getName() %>" />
    <jsp:param name="profileMap" value="true" />
    <jsp:param name="id" value="<%= animal.getId() %>" />
    <jsp:param name="breadcrumb" value="<%= breadcrumb %>"/>
</jsp:include>

<div id="profilePage" class="container_6 clearfix">
    <div class="grid_2">
        <div class ="profile">
            <h2><%= animal.getName() %></h2>
            <img id="ContentSubmission" src="<%= animal.getImageURL() %>"/><br/>
            <ul>
            <li>Wohnort: <%= animal.getPlace() %></li>
            <li>Halter: 
<%              ArrayList<String> owners = AnimalManagement.getOwners(animal.getId());
                if (null == owners || 0 == owners.size()) { %>
                    Dieses Tier hat noch keinen Halter. (Jetzt Halter werden)
<%              } else {
                    for (int i = 0; i < owners.size(); i++) { %>
                    <%= owners.get(i) %>
<%                      if (owners.size() < i) { %>
                    ,
<%                      }
                    }
                }
%>
            </li>
            </ul>
            <h3>Aufenthaltsort von <%= animal.getName() %></h3>
            <div id="locate" style="width: 256px; height: 256px"></div>
            <h3>Freunde von <%= animal.getName() %></h3>

            <jsp:include page="Friendlist.jsp?id=<%= animal.getId() %>" flush="true" />  

        </div>
    </div>
    <jsp:include page="SocialGraph.jsp" flush="true" >
        <jsp:param name="viewee" value="<%= animal.getId() %>"/>
    </jsp:include>



   <div id="postEvent" class="grid_3 event" >

<%
    ArrayList<AnimalBean> animals = UserManagement.getAnimals(currentUser.getId());
    if (animals == null) {
        throw new PetFinderException("Dashboard: An error occured during a database request.");
    }
    if (animals.size() == 0) {
%>
        <a href="Profile.jsp?id=" style="display:none;">
            <img height="96px" width="96px" src="http://imgur.com/iBwxT.jpg" />
        </a>

        <img class="speechballoon" src="images/speechballoon.png" />
        <form action="#" method="get" accept-charset="UTF-8">
            <textarea cols=32 rows=3 name="text" title="text"></textarea>
            post as&nbsp;
            <select name="cars">
                <option value="">You have no animals, yet.</option>
            </select>
            &nbsp;
            <input type="submit" value="send" />
        </form>
    </div>

<%
    } else {
        for (int i = 0; i < animals.size(); i++) {
            int id = animals.get(i).getId();
            String img = animals.get(i).getImageURL();
            String name = animals.get(i).getName();
            if (i == 0) { %>
        <a title="<%= name %>" href="Profile.jsp?id=<%= id %>" style="display:none;">
<%
            } else {
%>
        <a title="<%= name %>" href="Profile.jsp?id=<%= id %>" style="display:none;">
<%
            }
%>
            <img height="96px" width="96px" src="<%= img %>" alt ="<%= name %>" />
        </a>
<%      }
%>
        <img class="speechballoon" src="images/speechballoon.png" />
        <form id="wallPostForm" method="post" accept-charset="UTF-8">
            <input type="hidden" name="to" value="<%= animal.getId() %>"/>
            <textarea cols=32 rows=3 name="text" title="text"></textarea>
            <span>
            as&nbsp;
            
            <select name="from">
<%
            for (int i = 0; i < animals.size(); i++) {
                int id = animals.get(i).getId();
                String name = animals.get(i).getName();
                if (((Integer) session.getAttribute("aID")).equals(id)) {
%>
                <option title="<%= name %>" selected="true" value="<%= id %>"><%= name %></option>
<%
                } else {
%>
                <option title="<%= name %>" selected="false" value="<%= id %>"><%= name %></option>
<%
                }
            }
%>
            </select>
            </span>
            <input type="submit" value="Send" />
        </form>
    </div>
<%
   }
%>
    <div id ="eventStream">
        <jsp:include page="EventStream.jsp?type=0" flush="true"/>
    </div>

    <div class="grid_3 event">
        <div id="olderEvents">
            <a href="#olderEvents">older events</a>
        </div>
    </div>

</div>

<jsp:include page="index/afterContent.html" flush="true" />