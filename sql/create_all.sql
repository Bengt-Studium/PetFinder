create table users(
  id int generated always as identity primary key,
  name varchar(40) not null,
  password int not null,
  mailAdress varchar(40),
  isLoggedIn smallint not null
);

--create table photos(
--  url varchar(30) primary key,
--  size smallint not null,
--  deletionURL varchar(40)
--);

create table animals(
  id int generated always as identity primary key,
  name varchar(40) not null,
  pictureURL varchar(300) not null,
  place varchar(50) not null
);

create table locations(
  id bigint generated always as identity primary key,
  animalID int references animals(id) not null,
  timestamp timestamp not null,
  longitude float not null,
  latitude float not null
);

create table events(
  id int generated always as identity primary key,
  type smallint not null,
  timestamp timestamp not null,
  longitude float,
  latitude float,
  actor1 int references animals(id) not null,
  actor2 int not null,
  content varchar(1024)
);

create table animal_friend(
  aid int references animals(id) not null,
  fid int references animals(id) not null,
  acceptance smallint not null
);
alter table animal_friend add constraint animal_friend_pk primary key (aid, fid);

create table user_animal(
  uid int references users(id) not null,
  aid int references animals(id) not null
);
alter table user_animal add constraint user_animal_pk primary key (uid, aid);
